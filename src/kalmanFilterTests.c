#include "../include/KalmanFilter.h"
#include "../unity/unity.h"
#include <stdlib.h>

void test_createMatrixF_should_returnMatrixF() {
    Matrix *matrixF = createMatrixF(0.5f);
    float testMatrix[16] = {1, 0, (float) 0.5, 0, 0, 1, 0, (float) 0.5, 0, 0, 1, 0, 0, 0, 0, 1};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixF->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixF);
}

void test_createMatrixH_should_returnMatrixH() {
    Matrix *matrixH = createMatrixH();
    float testMatrix[8] = {1, 0, 0, 0, 0, 1, 0, 0};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixH->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixH);
}

void test_createMatrixB_should_returnMatrixB() {
    Matrix *matrixB = createMatrixB(0.5f);
    float testMatrix[4] = {0.125f, 0.125f, 0.5f, 0.5f};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixB->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixB);
}

void test_createMatrixQ_should_returnMatrixQ() {
    Matrix *matrixQ = createDiscreteWhiteNoiseQMatrix(1);
    float testMatrix[16] = {0.25f, 0.5f, 0, 0, 0.5f, 0.25f, 0.5f, 0, 0, 0.5f, 1, 0, 0, 0, 0, 1};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixQ->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixQ);
}

void test_createMatrixR_should_returnMatrixR() {
    Matrix *matrixR = createMatrixR(10, 20);
    float testMatrix[4] = {10, 0, 0, 20};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixR->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixR);
}

void test_createMatrixP_should_returnMatrixP() {
    Matrix *matrixP = createMatrixP(30, 30, 40, 40);
    float testMatrix[16] = {30, 0, 0, 0, 0, 30, 0, 0, 0, 0, 40, 0, 0, 0, 0, 40};
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixP->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixP);
}

void test_makePrediction_should_returnPredictionXMatrix() {
    float dt = 1;
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[4] = {(float) 6.5, (float) 8.5, 8, 10};
    Matrix *matrixF = createMatrixF(dt);
    Matrix *matrixB = createMatrixB(dt);
    Matrix *x = createMatrix(4, 1, error);
    x->matrix[0] = 0;
    x->matrix[1] = 0;
    x->matrix[2] = 5;
    x->matrix[3] = 7;
    float controlValue = 3;
    Matrix *resultMatrix = makePrediction(matrixF, x, matrixB, controlValue);
    TEST_ASSERT_EQUAL_INT(4, resultMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(1, resultMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, resultMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixF);
    freeMatrix(matrixB);
    freeMatrix(x);
    freeMatrix(resultMatrix);
    free(error);
}

void test_makeProjectCovariance_should_returnProjectCovarianceMatrixP(void) {
    float dt = 1;
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[16] = {20.25, 0.5, 10, 0, 0.5, 20.25, 0.5, 10, 10, 0.5, 11, 0, 0, 10, 0, 11};
    Matrix *matrixF = createMatrixF(dt);
    Matrix *matrixP = createMatrixP(10, 10, 10, 10);
    Matrix *matrixQ = createDiscreteWhiteNoiseQMatrix(dt);
    Matrix *matrixProjectP;
    matrixProjectP = makeProjectErrorCovariance(matrixF, matrixP, matrixQ);
    TEST_ASSERT_EQUAL_INT(4, matrixProjectP->amountOfColumns);
    TEST_ASSERT_EQUAL_INT(4, matrixProjectP->amountOfRows);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, matrixProjectP->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrixF);
    freeMatrix(matrixP);
    freeMatrix(matrixQ);
    freeMatrix(matrixProjectP);
    free(error);
}

void test_calculateKalmanCoefficient_should_returnKalmanCoefficient(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[8] = {0.6666667, 0, 0, 0.6666667, 0, 0, 0, 0};
    Matrix *projectCovarianceMatrixP = createMatrixP(10, 10, 10, 10);
    Matrix *matrixH = createMatrixH();
    Matrix *matrixR = createMatrixR(5, 5);
    Matrix *kalmanCoefficient;
    kalmanCoefficient = calculateKalmanCoefficient(projectCovarianceMatrixP, matrixH, matrixR);
    TEST_ASSERT_EQUAL_INT(4, kalmanCoefficient->amountOfRows);
    TEST_ASSERT_EQUAL_INT(2, kalmanCoefficient->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, kalmanCoefficient->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(projectCovarianceMatrixP);
    freeMatrix(matrixH);
    freeMatrix(matrixR);
    freeMatrix(kalmanCoefficient);
    free(error);
}

void test_updatePrediction_should_returnUpdatedPrediction(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[4] = {5, 2, 6, 0};
    Matrix *prediction = createMatrix(4, 1, error);
    prediction->matrix[0] = 7;
    prediction->matrix[1] = 5;
    prediction->matrix[2] = 7;
    prediction->matrix[3] = 5;
    Matrix *matrixH = createMatrixH();
    Matrix *measurement = createMatrix(2, 1, error);
    measurement->matrix[0] = 6;
    measurement->matrix[1] = 5;
    Matrix *kalmanCoefficient = createMatrix(4, 2, error);
    kalmanCoefficient->matrix[0] = 2;
    kalmanCoefficient->matrix[1] = 2;
    kalmanCoefficient->matrix[2] = 3;
    kalmanCoefficient->matrix[3] = 3;
    kalmanCoefficient->matrix[4] = 1;
    kalmanCoefficient->matrix[5] = 1;
    kalmanCoefficient->matrix[6] = 5;
    kalmanCoefficient->matrix[7] = 5;
    Matrix *updatedPrediction = updatePrediction(prediction, kalmanCoefficient, measurement, matrixH);
    TEST_ASSERT_EQUAL_INT(4, updatedPrediction->amountOfRows);
    TEST_ASSERT_EQUAL_INT(1, updatedPrediction->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, updatedPrediction->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(prediction);
    freeMatrix(matrixH);
    freeMatrix(measurement);
    freeMatrix(kalmanCoefficient);
    freeMatrix(updatedPrediction);
    free(error);
}

void test_updateErrorCovarianceMatrixP_should_returnUpdatedErrorCovarianceMatrixP(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[16] = {0, -10, 0, 0, -20, -10, 0, 0, -30, -30, 10, 0, -40, -40, 0, 10};
    Matrix *kalmanCoefficient = createMatrix(4, 2, error);
    kalmanCoefficient->matrix[0] = 1;
    kalmanCoefficient->matrix[1] = 1;
    kalmanCoefficient->matrix[2] = 2;
    kalmanCoefficient->matrix[3] = 2;
    kalmanCoefficient->matrix[4] = 3;
    kalmanCoefficient->matrix[5] = 3;
    kalmanCoefficient->matrix[6] = 4;
    kalmanCoefficient->matrix[7] = 4;
    Matrix *matrixH = createMatrixH();
    Matrix *projectErrorCovarianceMatrixP = createMatrixP(10, 10, 10, 10);
    Matrix *updatedErrorCovarianceMatrixP = updateErrorCovarianceMatrixP(kalmanCoefficient, matrixH,
                                                                         projectErrorCovarianceMatrixP);
    TEST_ASSERT_EQUAL_INT(4, updatedErrorCovarianceMatrixP->amountOfRows);
    TEST_ASSERT_EQUAL_INT(4, updatedErrorCovarianceMatrixP->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, updatedErrorCovarianceMatrixP->matrix,
                                  sizeof(testMatrix) / sizeof(float));
    freeMatrix(kalmanCoefficient);
    freeMatrix(matrixH);
    freeMatrix(projectErrorCovarianceMatrixP);
    freeMatrix(updatedErrorCovarianceMatrixP);
    free(error);
}

void test_oneStepKalmanFilter_should_returnArrayWithUpdatedPredictionAndUpdatedErrorCovarianceMatrixP(void) {
    float dt = 1;
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrixX[4] = {9.219378, 10.4214, 8.619104, 10.1572};
    float testMatrixP[16] = {4.009511, 0.01961359, 1.979, -0.0392272, 0.01961362, 4.009514, 0.059821153, 1.980975,
                             1.979, 0.05982148, 7.036, -0.119643,
                             -0.039227, 1.980975, -0.119643, 7.038};
    Matrix *results = malloc(sizeof(Matrix) * 2);
    Matrix *matrixF = createMatrixF(dt);
    Matrix *matrixX = createMatrix(4, 1, error);
    matrixX->matrix[0] = 0;
    matrixX->matrix[1] = 0;
    matrixX->matrix[2] = 5;
    matrixX->matrix[3] = 7;
    Matrix *matrixB = createMatrixB(dt);
    float controlValue = 2;
    Matrix *matrixP = createMatrixP(10, 10, 10, 10);
    Matrix *matrixQ = createDiscreteWhiteNoiseQMatrix(dt);
    Matrix *matrixH = createMatrixH();
    Matrix *matrixR = createMatrixR(5, 5);
    Matrix *measurement = createMatrix(2, 1, error);
    measurement->matrix[0] = 10;
    measurement->matrix[1] = 11;
    results = oneStepKalmanFilter(matrixF, matrixX, matrixB, controlValue, matrixP, matrixQ, matrixH, matrixR,
                                  measurement, results);
    Matrix *updatedPrediction = &results[0];
    Matrix *updatedErrorCovarianceMatrixP = &results[1];
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrixX, updatedPrediction->matrix, sizeof(testMatrixX) / sizeof(float));
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrixP, updatedErrorCovarianceMatrixP->matrix,
                                  sizeof(testMatrixP) / sizeof(float));
    free(error);
    freeMatrix(results);
    freeMatrix(matrixF);
    freeMatrix(matrixX);
    freeMatrix(matrixB);
    freeMatrix(matrixP);
    freeMatrix(matrixQ);
    freeMatrix(matrixH);
    freeMatrix(matrixR);
    freeMatrix(measurement);
}

void test_kalmanFilter_should_returnTwoCorrectFilteredValue(void) {
    float dt = 1;
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testFirstMatrixX[4] = {9.219378, 10.4214, 8.619104, 10.1572};
    float testSecondMatrix[4] = {16.84134, 19.776, 5.92489, 7.5564};
    float *controlValues = malloc(sizeof(float) * 2);
    controlValues[0] = 2;
    controlValues[1] = -3;
    Matrix *measurements = malloc(sizeof(Matrix) * 2);
    Matrix *measurement = createMatrix(2, 1, error);
    measurement->matrix[0] = 10;
    measurement->matrix[1] = 11;
    measurements[0] = *measurement;
    measurement = createMatrix(2, 1, error);
    measurement->matrix[0] = 17;
    measurement->matrix[1] = 20;
    measurements[1] = *measurement;
    Matrix *results = malloc(sizeof(Matrix) * 2);
    Matrix *matrixF = createMatrixF(dt);
    Matrix *matrixX = createMatrix(4, 1, error);
    matrixX->matrix[0] = 0;
    matrixX->matrix[1] = 0;
    matrixX->matrix[2] = 5;
    matrixX->matrix[3] = 7;
    Matrix *matrixB = createMatrixB(dt);
    Matrix *matrixP = createMatrixP(10, 10, 10, 10);
    Matrix *matrixQ = createDiscreteWhiteNoiseQMatrix(dt);
    Matrix *matrixH = createMatrixH();
    Matrix *matrixR = createMatrixR(5, 5);
    results = kalmanFilter(matrixF, matrixX, matrixB, controlValues, 2, matrixP, matrixQ, matrixH, matrixR,
                           measurements, results);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testFirstMatrixX, results[0].matrix, sizeof(testFirstMatrixX) / sizeof(float));
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testSecondMatrix, results[1].matrix, sizeof(testSecondMatrix) / sizeof(float));
    free(error);
    freeMatrix(results);
    freeMatrix(matrixF);
    freeMatrix(matrixX);
    freeMatrix(matrixB);
    freeMatrix(matrixP);
    freeMatrix(matrixQ);
    freeMatrix(matrixH);
    freeMatrix(matrixR);
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_createMatrixF_should_returnMatrixF);
    RUN_TEST(test_createMatrixH_should_returnMatrixH);
    RUN_TEST(test_createMatrixB_should_returnMatrixB);
    RUN_TEST(test_createMatrixQ_should_returnMatrixQ);
    RUN_TEST(test_createMatrixR_should_returnMatrixR);
    RUN_TEST(test_createMatrixP_should_returnMatrixP);
    RUN_TEST(test_makePrediction_should_returnPredictionXMatrix);
    RUN_TEST(test_makeProjectCovariance_should_returnProjectCovarianceMatrixP);
    RUN_TEST(test_calculateKalmanCoefficient_should_returnKalmanCoefficient);
    RUN_TEST(test_updatePrediction_should_returnUpdatedPrediction);
    RUN_TEST(test_updateErrorCovarianceMatrixP_should_returnUpdatedErrorCovarianceMatrixP);
    RUN_TEST(test_oneStepKalmanFilter_should_returnArrayWithUpdatedPredictionAndUpdatedErrorCovarianceMatrixP);
    RUN_TEST(test_kalmanFilter_should_returnTwoCorrectFilteredValue);
    return UNITY_END();
}
