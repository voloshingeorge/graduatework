#include "../include/Matrix.h"
#include "../unity/unity.h"
#include <stdio.h>
#include <stdlib.h>

void test_createMatrix_should_correctCreatingMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *newMatrix = createMatrix(2, 3, error);
    float testMatrix[6] = {0, 0, 0, 0, 0, 0};
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(2, newMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(3, newMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, newMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(newMatrix);
    free(error);
}

void
test_createMatrix_should_incorrectCreatingMatrixIfAmountOfColumnsOrRowsLessThanOneErrorMustBeEqualOneAndPointerToMatrixEqualNull(
        void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *newMatrix = createMatrix(0, 3, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, newMatrix);
    free(error);
}

void test_makeTheIdentityMatrix_should_correctReturnIdentityMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[9] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
    Matrix *identityMatrix = makeTheIdentityMatrix(createMatrix(3, 3, error), error, NULL);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(3, identityMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(3, identityMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, identityMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(identityMatrix);
    free(error);
}

void
test_makeTheIdentityMatrix_should_returnNullIfAmountOfColumnsOrRowsInMatrixForTransformationLessThanOneAndErrorMustBeEqualOne(
        void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *newMatrix = createMatrix(3, 3, error);
    newMatrix->amountOfColumns = 0;
    Matrix *identityMatrix = makeTheIdentityMatrix(newMatrix, error, NULL);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, identityMatrix);
    freeMatrix(newMatrix);
    free(error);
}

void
test_makeTheIdentityMatrix_should_returnNullIfPointerToMatrixForTransformationEqualNullAndErrorMustBeEqualTwo(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *identityMatrix = makeTheIdentityMatrix(NULL, error, NULL);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL(NULL, identityMatrix);
    free(error);
}

void
test_makeTheIdentityMatrix_should_returnNullIfMatrixForTransformationNotASquareMatrixAndErrorMustBeEqualThree(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *newMatrix = createMatrix(2, 4, error);
    Matrix *identityMatrix = makeTheIdentityMatrix(newMatrix, error, NULL);
    TEST_ASSERT_EQUAL_INT(3, *error);
    TEST_ASSERT_EQUAL(NULL, identityMatrix);
    freeMatrix(newMatrix);
    free(error);
}

void test_makeACopyFromMatrix_should_correctReturnCopyOfInputMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[9] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    Matrix *newMatrix = createMatrix(3, 3, error);
    for (int i = 0; i < 9; i++) {
        newMatrix->matrix[i] = (float) i;
    }
    Matrix *copyOfMatrix = makeACopyOfMatrix(newMatrix, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(3, copyOfMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(3, copyOfMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, copyOfMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    free(copyOfMatrix);
    freeMatrix(newMatrix);
    free(error);
}

void
test_makeACopyFromMatrix_should_returnNullIfAmountOfRowsOrColumnsInOriginalMatrixLessThanOneAndErrorMustBeEqualOne(
        void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *newMatrix = createMatrix(3, 3, error);
    newMatrix->amountOfColumns = 0;
    Matrix *copyOfMatrix = makeACopyOfMatrix(newMatrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, copyOfMatrix);
    freeMatrix(newMatrix);
    free(error);
}

void test_makeACopyFromMatrix_should_returnNullIfPointerToOriginalMatrixEqualNullAndErrorMustBeTwo(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *copyOfMatrix = makeACopyOfMatrix(NULL, error);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL(NULL, copyOfMatrix);
    free(error);
}

void test_sumMatrix_should_returnCorrectSummaryMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[2 * 4] = {4, 6, 8, 10, 12, 14, 16, 18};
    Matrix *firstMatrix = createMatrix(2, 4, error);
    Matrix *secondMatrix = createMatrix(2, 4, error);
    for (int i = 0; i < 2 * 4; i++) {
        firstMatrix->matrix[i] = (float) i;
        secondMatrix->matrix[i] = (float) (4 + i);
    }
    Matrix *summaryMatrix = sumMatrix(firstMatrix, secondMatrix, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(2, summaryMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(4, summaryMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, summaryMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(summaryMatrix);
    freeMatrix(firstMatrix);
    freeMatrix(secondMatrix);
    free(error);
}

void test_sumMatrix_should_returnNullIfSomeInputMatrixesHasAmountOfColumnsOrRowsLessThanOneAndErrorMustBeOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *firstMatrix = createMatrix(2, 4, error);
    Matrix *secondMatrix = createMatrix(2, 4, error);
    secondMatrix->amountOfRows = 0;
    Matrix *summaryMatrix = sumMatrix(firstMatrix, secondMatrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, summaryMatrix);
    freeMatrix(firstMatrix);
    freeMatrix(secondMatrix);
    free(error);
}

void test_sumMatrix_should_returnNullIfAmountOfRowsAndColumnsInMatrixesNotEqualAndErrorMustBeOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *firstMatrix = createMatrix(2, 4, error);
    Matrix *secondMatrix = createMatrix(2, 5, error);
    Matrix *summaryMatrix = sumMatrix(firstMatrix, secondMatrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, summaryMatrix);
    freeMatrix(firstMatrix);
    freeMatrix(secondMatrix);
    free(error);
}

void test_sumMatrix_should_returnNullIfSomePointersOfInputMatrixesEqualNullAndErrorMustBeTwo(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *firstMatrix = createMatrix(2, 4, error);
    Matrix *summaryMatrix = sumMatrix(firstMatrix, NULL, error);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL(NULL, summaryMatrix);
    freeMatrix(firstMatrix);
    free(error);
}

void test_differenceMatrix_should_returnCorrectDifferenceMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[16] = {5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10};
    Matrix *firstMatrix = createMatrix(4, 4, error);
    Matrix *secondMatrix = createMatrix(4, 4, error);
    for (int i = 0; i < firstMatrix->amountOfRows * firstMatrix->amountOfColumns; i++) {
        firstMatrix->matrix[i] = (float) i + 5;
        secondMatrix->matrix[i] = (float) i + (float) i;
    }
    Matrix *resultMatrix;
    resultMatrix = differenceMatrix(firstMatrix, secondMatrix, error);
    TEST_ASSERT_EQUAL_INT(4, resultMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(4, resultMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, resultMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(firstMatrix);
    freeMatrix(secondMatrix);
    freeMatrix(resultMatrix);
}

void test_transposeMatrix_should_returnCorrectTransposedMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[8] = {0, 4, 1, 5, 2, 6, 3, 7};
    Matrix *newMatrix = createMatrix(2, 4, error);
    for (int i = 0; i < 2 * 4; i++) {
        newMatrix->matrix[i] = (float) i;
    }
    Matrix *transposedMatrix = transposeMatrix(newMatrix, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(4, transposedMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(2, transposedMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, transposedMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(transposedMatrix);
    freeMatrix(newMatrix);
    free(error);
}

void test_transposeMatrix_should_returnNullIfAmountOfColumnsOrRowsLessThanOneAndErrorMustBeOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *newMatrix = createMatrix(2, 4, error);
    newMatrix->amountOfRows = 0;
    Matrix *transposedMatrix = transposeMatrix(newMatrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, transposedMatrix);
    free(newMatrix);
    free(error);
}

void test_transposeMatrix_should_returnNullIfPointerToMatrixForTransposeEqualNullAndErrorMustBeTwo(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *transposedMatrix = transposeMatrix(NULL, error);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL(NULL, transposedMatrix);
    free(error);
}

void test_mulMatrix_should_returnCorrectResultFromMultiplicationOfMatrixes(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[2 * 3] = {324, 414, 516, 828, 1078, 1372};
    Matrix *firstMatrix = createMatrix(2, 4, error);
    for (int i = 0; i < 2 * 4; i++) {
        firstMatrix->matrix[i] = (float) i;
    }
    Matrix *secondMatrix = createMatrix(4, 3, error);
    for (int i = 0; i < 4 * 3; i++) {
        secondMatrix->matrix[i] = (float) (i * i);
    }
    Matrix *resultMatrix = mulMatrix(firstMatrix, secondMatrix, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(2, resultMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(3, resultMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, resultMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(firstMatrix);
    freeMatrix(secondMatrix);
    freeMatrix(resultMatrix);
    free(error);
}

void test_mulMatrix_should_returnNullIfOneOfMatrixHasAmountOfColumnsOrRowsLessThanOneAndErrorMustBeEqualOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *firstMatrix = createMatrix(2, 4, error);
    Matrix *secondMatrix = createMatrix(4, 3, error);
    firstMatrix->amountOfColumns = 0;
    secondMatrix->amountOfRows = 0;
    Matrix *resultMatrix = mulMatrix(firstMatrix, secondMatrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, resultMatrix);
    freeMatrix(firstMatrix);
    freeMatrix(secondMatrix);
    free(error);
}

void test_mulMatrix_should_returnNullIfPointersOfSomeMatrixEqualNullAndErrorMustBeEqualTwo(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *firstMatrix = createMatrix(2, 4, error);
    Matrix *resultMatrix = mulMatrix(firstMatrix, NULL, error);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL(NULL, resultMatrix);
    freeMatrix(firstMatrix);
    free(error);
}

void test_mulMatrixAndValue_should_returnCorrectResultFromMultiplicationOfMatrixAndValue(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[4] = {0, 3, 6, 9};
    Matrix *matrix = createMatrix(2, 2, error);
    for (int i = 0; i < matrix->amountOfRows + matrix->amountOfColumns; i++) {
        matrix->matrix[i] = (float) i;
    }
    Matrix *resultMatrix = createMatrix(2, 2, error);
    resultMatrix = mulMatrixAndValue(matrix, (float) 3, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(2, resultMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(2, resultMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, resultMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(matrix);
    free(error);
}

void test_matrixDeterminant_should_returnCorrectDeterminantOfMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrix = createMatrix(3, 3, error);
    for (int i = 0; i < 3 * 3; i++) {
        matrix->matrix[i] = (float) -i * (2 + (float) i) + (float) i - 1;
    }
    matrix->matrix[0] = 0;
    double determinant = matrixDeterminant(matrix, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_FLOAT(-18, determinant);
    freeMatrix(matrix);
    free(error);
}

void
test_matrixDeterminant_should_returnNullIfAmountOfRowsOrColumnsInInputMatrixLessThanOneAndErrorMustBeEqualOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrix = createMatrix(3, 3, error);
    matrix->amountOfRows = 0;
    float determinant = matrixDeterminant(matrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL_FLOAT(0.0, determinant);
    freeMatrix(matrix);
    free(error);
}

void test_matrixDeterminant_should_returnNullIfInputMatrixIsNotSquareAndErrorMustBeEqualThree(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrix = createMatrix(3, 4, error);
    float determinant = matrixDeterminant(matrix, error);
    TEST_ASSERT_EQUAL_INT(3, *error);
    TEST_ASSERT_EQUAL_FLOAT(0.0, determinant);
    freeMatrix(matrix);
    free(error);
}

void test_matrixDeterminant_should_returnNullIfPointerToInputMatrixEqualNullAndErrorMustBeEqualTwo(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float determinant = matrixDeterminant(NULL, error);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL_FLOAT(0.0, determinant);
    free(error);
}

void test_inverseMatrix_should_returnCorrectInverseMatrix(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    float testMatrix[3 * 3] = {13, -10, 3, -21.333333, 16.722222, -5.0555555, 9, -7.166666, 2.166666};
    Matrix *matrix = createMatrix(3, 3, error);
    for (int i = 0; i < 3 * 3; i++) {
        matrix->matrix[i] = (float) -i * (2 + (float) i) + (float) i - 1;
    }
    matrix->matrix[0] = 0;
    Matrix *invMatrix = inverseMatrix(matrix, error);
    TEST_ASSERT_EQUAL_INT(0, *error);
    TEST_ASSERT_EQUAL_INT(3, invMatrix->amountOfRows);
    TEST_ASSERT_EQUAL_INT(3, invMatrix->amountOfColumns);
    TEST_ASSERT_EQUAL_FLOAT_ARRAY(testMatrix, invMatrix->matrix, sizeof(testMatrix) / sizeof(float));
    freeMatrix(invMatrix);
    freeMatrix(matrix);
    free(error);
}

void test_inverseMatrix_should_returnNullIfAmountOfRowsOrColumnsLessThanOneAndErrorMustBeEqualOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrix = createMatrix(3, 3, error);
    matrix->amountOfRows = 0;
    Matrix *invMatrix = inverseMatrix(matrix, error);
    TEST_ASSERT_EQUAL_INT(1, *error);
    TEST_ASSERT_EQUAL(NULL, invMatrix);
    freeMatrix(matrix);
    free(error);
}

void test_inverseMatrix_should_returnNullIfInputMatrixIsNotSquareAndErrorMustBeEqualOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrix = createMatrix(3, 4, error);
    Matrix *invMatrix = inverseMatrix(matrix, error);
    TEST_ASSERT_EQUAL_INT(3, *error);
    TEST_ASSERT_EQUAL(NULL, invMatrix);
    freeMatrix(matrix);
    free(error);
}

void test_inverseMatrix_should_returnNullIfPointerToInputMatrixEqualNullAndErrorMustBeEqualOne(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *invMatrix = inverseMatrix(NULL, error);
    TEST_ASSERT_EQUAL_INT(2, *error);
    TEST_ASSERT_EQUAL(NULL, invMatrix);
    free(error);
}

void test_inverseMatrix_should_returnNullIfDeterminantIsEqualZeroAndErrorMustBeEqualFour(void) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrix = createMatrix(3, 3, error);
    for (int i = 0; i < 3 * 3; i++) {
        matrix->matrix[i] = (float) i;
    }
    Matrix *invMatrix = inverseMatrix(matrix, error);
    TEST_ASSERT_EQUAL_INT(4, *error);
    TEST_ASSERT_EQUAL(NULL, invMatrix);
    freeMatrix(matrix);
    free(error);
}

/*int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_createMatrix_should_correctCreatingMatrix);
    RUN_TEST(
            test_createMatrix_should_incorrectCreatingMatrixIfAmountOfColumnsOrRowsLessThanOneErrorMustBeEqualOneAndPointerToMatrixEqualNull);
    RUN_TEST(test_makeTheIdentityMatrix_should_correctReturnIdentityMatrix);
    RUN_TEST(
            test_makeTheIdentityMatrix_should_returnNullIfAmountOfColumnsOrRowsInMatrixForTransformationLessThanOneAndErrorMustBeEqualOne);
    RUN_TEST(
            test_makeTheIdentityMatrix_should_returnNullIfMatrixForTransformationNotASquareMatrixAndErrorMustBeEqualThree);
    RUN_TEST(
            test_makeTheIdentityMatrix_should_returnNullIfPointerToMatrixForTransformationEqualNullAndErrorMustBeEqualTwo);
    RUN_TEST(test_makeACopyFromMatrix_should_correctReturnCopyOfInputMatrix);
    RUN_TEST(
            test_makeACopyFromMatrix_should_returnNullIfAmountOfRowsOrColumnsInOriginalMatrixLessThanOneAndErrorMustBeEqualOne);
    RUN_TEST(test_makeACopyFromMatrix_should_returnNullIfPointerToOriginalMatrixEqualNullAndErrorMustBeTwo);
    RUN_TEST(test_sumMatrix_should_returnCorrectSummaryMatrix);
    RUN_TEST(test_sumMatrix_should_returnNullIfSomeInputMatrixesHasAmountOfColumnsOrRowsLessThanOneAndErrorMustBeOne);
    RUN_TEST(test_sumMatrix_should_returnNullIfAmountOfRowsAndColumnsInMatrixesNotEqualAndErrorMustBeOne);
    RUN_TEST(test_sumMatrix_should_returnNullIfSomePointersOfInputMatrixesEqualNullAndErrorMustBeTwo);
    RUN_TEST(test_differenceMatrix_should_returnCorrectDifferenceMatrix);
    RUN_TEST(test_transposeMatrix_should_returnCorrectTransposedMatrix);
    RUN_TEST(test_transposeMatrix_should_returnNullIfAmountOfColumnsOrRowsLessThanOneAndErrorMustBeOne);
    RUN_TEST(test_transposeMatrix_should_returnNullIfPointerToMatrixForTransposeEqualNullAndErrorMustBeTwo);
    RUN_TEST(test_mulMatrix_should_returnCorrectResultFromMultiplicationOfMatrixes);
    RUN_TEST(test_mulMatrix_should_returnNullIfOneOfMatrixHasAmountOfColumnsOrRowsLessThanOneAndErrorMustBeEqualOne);
    RUN_TEST(test_mulMatrix_should_returnNullIfPointersOfSomeMatrixEqualNullAndErrorMustBeEqualTwo);
    RUN_TEST(test_mulMatrixAndValue_should_returnCorrectResultFromMultiplicationOfMatrixAndValue);
    RUN_TEST(test_matrixDeterminant_should_returnCorrectDeterminantOfMatrix);
    RUN_TEST(
            test_matrixDeterminant_should_returnNullIfAmountOfRowsOrColumnsInInputMatrixLessThanOneAndErrorMustBeEqualOne);
    RUN_TEST(test_matrixDeterminant_should_returnNullIfInputMatrixIsNotSquareAndErrorMustBeEqualThree);
    RUN_TEST(test_matrixDeterminant_should_returnNullIfPointerToInputMatrixEqualNullAndErrorMustBeEqualTwo);
    RUN_TEST(test_inverseMatrix_should_returnCorrectInverseMatrix);
    RUN_TEST(test_inverseMatrix_should_returnNullIfAmountOfRowsOrColumnsLessThanOneAndErrorMustBeEqualOne);
    RUN_TEST(test_inverseMatrix_should_returnNullIfInputMatrixIsNotSquareAndErrorMustBeEqualOne);
    RUN_TEST(test_inverseMatrix_should_returnNullIfPointerToInputMatrixEqualNullAndErrorMustBeEqualOne);
    RUN_TEST(test_inverseMatrix_should_returnNullIfDeterminantIsEqualZeroAndErrorMustBeEqualFour);
    return UNITY_END();
}*/
