#include "../include/random.h"

//Преобразование Бокса-Мюллера
float *normalRandom(float m, float sigma, float *randomNumber, int i) {
    float x, y, s, coef;
    srand(i);
    do {
        x = 2.0 * rand() / RAND_MAX - 1.0;
        y = 2.0 * rand() / RAND_MAX - 1.0;
        s = x * x + y * y;
    } while (s >= 1);
    coef = sqrtf(-2.0 * logf(s) / s);
    randomNumber[0] = m + sigma * x * coef;
    randomNumber[1] = m + sigma * y * coef;
    return randomNumber;
}
