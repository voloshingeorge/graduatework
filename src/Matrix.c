#include "../include/Matrix.h"
#include <stdio.h>
#include <stdlib.h>

/* Errors code */
// 1 - incorrect matrix size
// 2 - pointer to matrix equal NULL
// 3 - matrix is not square
// 4 - determinant equal zero

Matrix *createMatrix(unsigned short int amountOfRows, unsigned short int amountOfColumns, int *error) {
    if (amountOfRows < 1 || amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *newZeroMatrix = (Matrix *) malloc(sizeof(Matrix));
        newZeroMatrix->amountOfRows = amountOfRows;
        newZeroMatrix->amountOfColumns = amountOfColumns;
        newZeroMatrix->matrix = (float *) calloc(amountOfRows * amountOfColumns, sizeof(float));
        return newZeroMatrix;
    }
}

Matrix *createAndFillMatrixWithUserParametersAndUserValues(int *error) {
    unsigned short int amountOfRows = 0;
    unsigned short int amountOfColumns = 0;
    printf_s("Введите количество строк:");
    scanf_s("%hi", &amountOfRows);
    printf_s("Введите количество столбцов:");
    scanf_s("%hi", &amountOfColumns);
    if (amountOfRows < 1 || amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *newMatrix = createMatrix(amountOfRows, amountOfColumns, error);
        for (int i = 0; i < amountOfColumns * amountOfRows; i++) {
            printf_s("Введите %d значение:", i + 1);
            scanf_s("%f", &newMatrix->matrix[i]);
        }
        return newMatrix;
    }
}

Matrix *createMatrixWithUserParameters(int *error) {
    unsigned short int amountOfRows;
    unsigned short int amountOfColumns;
    printf_s("Введите количество строк:");
    scanf_s("%hi", &amountOfRows);
    printf_s("Введите количество столбцов:");
    scanf_s("%hi", &amountOfColumns);
    if (amountOfRows < 1 || amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        return createMatrix(amountOfRows, amountOfColumns, error);
    }
}

Matrix *makeTheIdentityMatrix(Matrix *matrixForTransformation, int *error) {
    if (matrixForTransformation == NULL) {
        *error = 2;
        return NULL;
    } else if (matrixForTransformation->amountOfRows < 1 || matrixForTransformation->amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else if (matrixForTransformation->amountOfRows != matrixForTransformation->amountOfColumns) {
        *error = 3;
        return NULL;
    } else {
        for (int i = 0; i < matrixForTransformation->amountOfRows; i++) {
            for (int j = 0; j < matrixForTransformation->amountOfColumns; j++) {
                if (i == j) {
                    matrixForTransformation->matrix[i * matrixForTransformation->amountOfRows + j] = 1;
                } else {
                    matrixForTransformation->matrix[i * matrixForTransformation->amountOfRows + j] = 0;
                }
            }
        }
        return matrixForTransformation;
    }
}

Matrix *makeACopyOfMatrix(Matrix *originalMatrix, int *error) {
    if (originalMatrix == NULL) {
        *error = 2;
        return NULL;
    } else if (originalMatrix->amountOfRows < 1 || originalMatrix->amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *copyOfOriginalMatrix = createMatrix(originalMatrix->amountOfRows, originalMatrix->amountOfColumns,
                                                    error);
        for (int i = 0; i < originalMatrix->amountOfRows * originalMatrix->amountOfColumns; i++) {
            copyOfOriginalMatrix->matrix[i] = originalMatrix->matrix[i];
        }
        return copyOfOriginalMatrix;
    }
}

Matrix *sumMatrix(Matrix *firstMatrix, Matrix *secondMatrix, int *error) {
    if (firstMatrix == NULL || secondMatrix == NULL) {
        *error = 2;
        return NULL;
    } else if (firstMatrix->amountOfColumns != secondMatrix->amountOfColumns ||
               firstMatrix->amountOfRows != secondMatrix->amountOfRows || firstMatrix->amountOfRows < 1 ||
               firstMatrix->amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *summedMatrix = createMatrix(firstMatrix->amountOfRows, firstMatrix->amountOfColumns, error);
        for (int i = 0; i < firstMatrix->amountOfRows * firstMatrix->amountOfColumns; i++) {
            summedMatrix->matrix[i] = firstMatrix->matrix[i] + secondMatrix->matrix[i];
        }
        return summedMatrix;
    }
}

Matrix *differenceMatrix(Matrix *firstMatrix, Matrix *secondMatrix, int *error) {
    if (firstMatrix == NULL || secondMatrix == NULL) {
        *error = 2;
        return NULL;
    } else if (firstMatrix->amountOfColumns != secondMatrix->amountOfColumns ||
               firstMatrix->amountOfRows != secondMatrix->amountOfRows || firstMatrix->amountOfRows < 1 ||
               firstMatrix->amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *resultMatrix = createMatrix(firstMatrix->amountOfRows, firstMatrix->amountOfColumns, error);
        for (int i = 0; i < firstMatrix->amountOfRows * firstMatrix->amountOfColumns; i++) {
            resultMatrix->matrix[i] = firstMatrix->matrix[i] - secondMatrix->matrix[i];
        }
        return resultMatrix;
    }
}

Matrix *transposeMatrix(Matrix *matrix, int *error) {
    if (matrix == NULL) {
        *error = 2;
        return NULL;
    } else if (matrix->amountOfRows < 1 || matrix->amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *newMatrix = createMatrix(matrix->amountOfColumns, matrix->amountOfRows, error);
        for (int i = 0; i < matrix->amountOfRows; i++) {
            for (int j = 0; j < matrix->amountOfColumns; j++) {
                newMatrix->matrix[j * newMatrix->amountOfColumns + i] = matrix->matrix[i * matrix->amountOfColumns + j];
            }
        }
        return newMatrix;
    }
}

Matrix *mulMatrix(Matrix *firstMatrix, Matrix *secondMatrix, int *error) {
    if (firstMatrix == NULL || secondMatrix == NULL) {
        *error = 2;
        return NULL;
    } else if (firstMatrix->amountOfColumns != secondMatrix->amountOfRows || firstMatrix->amountOfRows < 1 ||
               firstMatrix->amountOfColumns < 1 || secondMatrix->amountOfColumns < 1) {
        *error = 1;
        return NULL;
    } else {
        Matrix *resultMatrix = createMatrix(firstMatrix->amountOfRows, secondMatrix->amountOfColumns, error);
        if (firstMatrix->amountOfColumns == secondMatrix->amountOfRows) {
            float bufSum;
            for (int i = 0; i < resultMatrix->amountOfRows; i++) {
                for (int j = 0; j < resultMatrix->amountOfColumns; j++) {
                    bufSum = (float) 0.0;
                    for (int k = 0; k < firstMatrix->amountOfColumns; k++) {
                        bufSum += (firstMatrix->matrix[i * firstMatrix->amountOfColumns + k] *
                                   secondMatrix->matrix[k * secondMatrix->amountOfColumns + j]);
                    }
                    resultMatrix->matrix[i * resultMatrix->amountOfColumns + j] = bufSum;
                }
            }
        }
        return resultMatrix;
    }
}

Matrix *mulMatrixAndValue(Matrix *matrix, float value, int *error) {
    if (matrix == NULL) {
        *error = 2;
        return NULL;
    } else {
        Matrix *resultMatrix = makeACopyOfMatrix(matrix, error);
        for (int i = 0; i < resultMatrix->amountOfColumns + resultMatrix->amountOfRows; i++) {
            resultMatrix->matrix[i] *= value;
        }
        return resultMatrix;
    }
}

float matrixDeterminant(Matrix *matrix, int *error) {
    if (matrix == NULL) {
        *error = 2;
        return (float) 0.0;
    } else if (matrix->amountOfRows < 1 || matrix->amountOfColumns < 1) {
        *error = 1;
        return (float) 0.0;
    } else if (matrix->amountOfRows != matrix->amountOfColumns) {
        *error = 3;
        return (float) 0.0;
    } else {
        Matrix *copyOfMatrix = makeACopyOfMatrix(matrix, error);
        float determinant = (float) 1.0;
        for (int i = 0; i < copyOfMatrix->amountOfRows; i++) {
            if (copyOfMatrix->matrix[i * copyOfMatrix->amountOfColumns + i] == 0) {
                for (int l = i; l < copyOfMatrix->amountOfRows; l++) {
                    if (copyOfMatrix->matrix[l * copyOfMatrix->amountOfColumns + i] != 0) {
                        float temp;
                        for (int k = 0; k < copyOfMatrix->amountOfColumns; k++) {
                            temp = copyOfMatrix->matrix[i * copyOfMatrix->amountOfColumns + k];
                            copyOfMatrix->matrix[i * copyOfMatrix->amountOfColumns + k] = copyOfMatrix->matrix[
                                    l * copyOfMatrix->amountOfColumns + k];
                            copyOfMatrix->matrix[l * copyOfMatrix->amountOfColumns + k] = temp;
                        }
                    }
                }
            }
            for (int j = i; j < copyOfMatrix->amountOfRows; j++) {
                if (j != i) {
                    float temp = copyOfMatrix->matrix[j * copyOfMatrix->amountOfColumns + i] /
                                 copyOfMatrix->matrix[i * copyOfMatrix->amountOfColumns + i];
                    for (int k = 0; k < copyOfMatrix->amountOfColumns; k++) {
                        copyOfMatrix->matrix[j * copyOfMatrix->amountOfColumns + k] -=
                                copyOfMatrix->matrix[i * copyOfMatrix->amountOfColumns +
                                                     k] * temp;
                    }
                }
            }
        }
        for (int i = 0; i < copyOfMatrix->amountOfRows; i++) {
            determinant *= copyOfMatrix->matrix[i * copyOfMatrix->amountOfColumns + i];
        }
        return determinant;
    }
}

Matrix *inverseMatrix(Matrix *originalMatrix, int *error) {
    if (originalMatrix == NULL) {
        *error = 2;
        return NULL;
    } else if (originalMatrix->amountOfColumns < 1 || originalMatrix->amountOfRows < 1) {
        *error = 1;
        return NULL;
    } else if (originalMatrix->amountOfRows != originalMatrix->amountOfColumns) {
        *error = 3;
        return NULL;
    } else {
        float determinant = matrixDeterminant(originalMatrix, error);
        if (*error != 0) {
            return NULL;
        } else if (*error == 0 && determinant == 0) {
            *error = 4;
            return NULL;
        } else {
            Matrix *roughMatrix = makeACopyOfMatrix(originalMatrix, error);
            Matrix *inverseMatrix = makeTheIdentityMatrix(
                    createMatrix(originalMatrix->amountOfRows, originalMatrix->amountOfColumns, error), error);
            for (int i = 0; i < roughMatrix->amountOfRows; i++) {
                if (roughMatrix->matrix[i * roughMatrix->amountOfColumns + i] == 0) {
                    for (int l = i; l < roughMatrix->amountOfRows; l++) {
                        if (roughMatrix->matrix[l * roughMatrix->amountOfColumns + i] != 0) {
                            float temp;
                            for (int k = 0; k < roughMatrix->amountOfColumns; k++) {
                                temp = roughMatrix->matrix[i * roughMatrix->amountOfColumns + k];
                                roughMatrix->matrix[i * roughMatrix->amountOfColumns + k] = roughMatrix->matrix[
                                        l * roughMatrix->amountOfColumns + k];
                                roughMatrix->matrix[l * roughMatrix->amountOfColumns + k] = temp;

                                temp = inverseMatrix->matrix[i * inverseMatrix->amountOfColumns + k];
                                inverseMatrix->matrix[i * inverseMatrix->amountOfColumns + k] = inverseMatrix->matrix[
                                        l * roughMatrix->amountOfColumns + k];
                                inverseMatrix->matrix[l * roughMatrix->amountOfColumns + k] = temp;
                            }
                        }
                    }
                }
                for (int j = 0; j < roughMatrix->amountOfRows; j++) {
                    if (j != i) {
                        float temp = roughMatrix->matrix[j * roughMatrix->amountOfColumns + i] /
                                     roughMatrix->matrix[i * roughMatrix->amountOfColumns + i];
                        for (int k = 0; k < roughMatrix->amountOfColumns; k++) {
                            roughMatrix->matrix[j * roughMatrix->amountOfColumns + k] -=
                                    roughMatrix->matrix[i * roughMatrix->amountOfColumns + k] * temp;
                            inverseMatrix->matrix[j * inverseMatrix->amountOfColumns + k] -=
                                    inverseMatrix->matrix[i * inverseMatrix->amountOfColumns + k] * temp;
                        }
                    }
                }
            }
            for (int i = 0; i < roughMatrix->amountOfRows; i++) {
                float a = roughMatrix->matrix[i * roughMatrix->amountOfColumns + i];
                for (int j = 0; j < roughMatrix->amountOfColumns; j++) {
                    roughMatrix->matrix[i * roughMatrix->amountOfColumns + j] /= a;
                    inverseMatrix->matrix[i * inverseMatrix->amountOfColumns + j] /= a;
                }
            }
            freeMatrix(roughMatrix);
            return inverseMatrix;
        }
    }
}


void printMatrix(Matrix *matrixForPrinting) {
    for (int i = 0; i < matrixForPrinting->amountOfRows; i++) {
        for (int j = 0; j < matrixForPrinting->amountOfColumns; j++) {
            printf_s("%3.2f   ", matrixForPrinting->matrix[i * matrixForPrinting->amountOfColumns + j]);
        }
        printf_s("\n");
    }
}

void freeMatrix(Matrix *matrix) {
    free(matrix->matrix);
    free(matrix);
}

