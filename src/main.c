#include <stdio.h>
#include "../include/utils.h"
#include "../include/random.h"
#include "../include/KalmanFilter.h"


int main(void) {
    int amount = 100;
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    FILE *fout;
    fopen_s(&fout, "data.txt", "w");
    float dt = 1;
    float *randomSystemValues = malloc(sizeof(float) * 2);
    float *randomMeasurementValues = malloc(sizeof(float) * 2);
    float *controlValues = calloc(100, sizeof(float));
    Matrix *measurements = malloc(sizeof(Matrix) * 100);
    Matrix *realX = malloc(sizeof(Matrix) * 101);
    Matrix *filteredValues = malloc(sizeof(Matrix) * 100);
    Matrix *matrixF = createMatrixF(dt);
    Matrix *matrixB = createMatrixB(dt);
    Matrix *matrixX = createMatrix(4, 1, error);
    Matrix *matrixP = createMatrixP(10, 10, 10, 10);
    Matrix *matrixQ = createDiscreteWhiteNoiseQMatrix(dt);
    Matrix *matrixH = createMatrixH();
    Matrix *matrixR = createMatrixR(400, 400);
    matrixX->matrix[0] = 0;
    matrixX->matrix[1] = 0;
    matrixX->matrix[2] = 8;
    matrixX->matrix[3] = 12;
    realX[0] = *makeACopyOfMatrix(matrixX, error);
    for (int i = 0; i < amount; i++) {
        //Высчитывание данных X по модели процесса
        Matrix *realMatrix = createMatrix(4, 1, error);
        realMatrix->matrix[0] = realX[i].matrix[0] + matrixX->matrix[2];
        realMatrix->matrix[1] = realX[i].matrix[1] + matrixX->matrix[3];
        realMatrix->matrix[2] = matrixX->matrix[2];
        realMatrix->matrix[3] = matrixX->matrix[3];
        //Добавляем управляющее воздействие
        realMatrix = sumMatrix(realMatrix, mulMatrixAndValue(matrixB, controlValues[i], error), error);
        //Добавляем шумы системы
        randomSystemValues = normalRandom(0, 5, randomSystemValues, i);
        realMatrix->matrix[0] = realMatrix->matrix[0] + randomSystemValues[0];
        realMatrix->matrix[1] = realMatrix->matrix[1] + randomSystemValues[1];
        randomSystemValues = normalRandom(0, 5, randomSystemValues, i);
        realMatrix->matrix[2] = realMatrix->matrix[2] + randomSystemValues[0];
        realMatrix->matrix[3] = realMatrix->matrix[3] + randomSystemValues[1];
        //Копируем реальные значения X в наблюдения
        Matrix *measurementMatrix = createMatrix(2, 1, error);
        //Добавляем к рельным значениям X шумы наблюдений
        randomMeasurementValues = normalRandom(0, 20, randomMeasurementValues, i);
        measurementMatrix->matrix[0] = realMatrix->matrix[0] + randomMeasurementValues[0];
        measurementMatrix->matrix[1] = realMatrix->matrix[1] + randomMeasurementValues[1];
        realX[i + 1] = *makeACopyOfMatrix(realMatrix, error);
        measurements[i] = *makeACopyOfMatrix(measurementMatrix, error);
    }
    filteredValues = kalmanFilter(matrixF, matrixX, matrixB, controlValues, 100, matrixP, matrixQ, matrixH, matrixR,
                                  measurements, filteredValues);
    for (int i = 0; i < amount; i++) {
        printMatrixInFile(fout, &realX[i]);
        printMatrixInFile(fout, &measurements[i]);
        printMatrixInFile(fout, &filteredValues[i]);
    }
    fclose(fout);
    return 0;
}
