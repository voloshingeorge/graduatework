#include "../include/utils.h"

void printMatrixInFile(FILE *fout, Matrix *matrix) {
    for (int i = 0; i < matrix->amountOfRows * matrix->amountOfColumns; i++) {
        fprintf_s(fout, "%f", matrix->matrix[i]);
        fprintf_s(fout, "%c", ' ');
    }
    fprintf_s(fout, "%c", '\n');
}

float myPow(float basis, int exponent) {
    float finalValue = 1;
    if (exponent == 0) {
        return finalValue;
    } else {
        for (int i = 0; i < exponent; i++) {
            finalValue *= basis;
        }
        return finalValue;
    }
}
