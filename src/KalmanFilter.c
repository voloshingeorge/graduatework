#include "../include/KalmanFilter.h"

/* Генерирует матрицу F:
 * [1, 0, dt, 0,
 *  0, 1, 0, dt,
 *  0, 0, 1, 0,
 *  0, 0, 0, 1]
*/
Matrix *createMatrixF(float dt) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrixF = createMatrix(4, 4, error);
    matrixF->matrix[0] = 1;
    matrixF->matrix[2] = dt;
    matrixF->matrix[5] = 1;
    matrixF->matrix[7] = dt;
    matrixF->matrix[10] = 1;
    matrixF->matrix[15] = 1;
    return matrixF;
}

/* Генерирует матрицу H:
 * [1, 0, 0, 0,
 *  0, 1, 0, 0]
*/
Matrix *createMatrixH() {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrixH = createMatrix(2, 4, error);
    matrixH->matrix[0] = 1;
    matrixH->matrix[5] = 1;
    return matrixH;
}

/* Генерирует матрицу B:
 * [(dt^2)/2, dt]
*/
Matrix *createMatrixB(float dt) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrixB = createMatrix(4, 1, error);
    matrixB->matrix[0] = myPow(dt, 2) / 2;
    matrixB->matrix[1] = myPow(dt, 2) / 2;
    matrixB->matrix[2] = dt;
    matrixB->matrix[3] = dt;
    return matrixB;
}

/* Генерирует матрицу Q для ситуации [x y x' y']
 * [0.25*dt^4, 0.5*dt^3,  0,        0,
 *  0.5*dt^3,  0.25*dt^4, 0.5*dt^3, 0,
 *  0,         0.5*dt^3,  dt^2,     0,
 *  0,         0,         0,        dt^2]
 */
Matrix *createDiscreteWhiteNoiseQMatrix(float dt) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrixQ = createMatrix(4, 4, error);
    matrixQ->matrix[0] = 0.25f * myPow(dt, 4);
    matrixQ->matrix[1] = 0.5f * myPow(dt, 3);
    matrixQ->matrix[4] = 0.5f * myPow(dt, 3);
    matrixQ->matrix[5] = 0.25f * myPow(dt, 4);
    matrixQ->matrix[6] = 0.5f * myPow(dt, 3);
    matrixQ->matrix[9] = 0.5f * myPow(dt, 3);
    matrixQ->matrix[10] = myPow(dt, 2);
    matrixQ->matrix[15] = myPow(dt, 2);
    return matrixQ;
}

/* Генерирует матрицу R:
 * [disp1, 0,
 *  0,       disp2]
 */
Matrix *createMatrixR(float disp1, float disp2) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrixR = createMatrix(2, 2, error);
    matrixR->matrix[0] = disp1;
    matrixR->matrix[3] = disp2;
    return matrixR;
}

/* Генерирует матрицу P:
 * [disp1, 0,
 *  0,       disp2]
 */
Matrix *createMatrixP(float disp1, float disp2, float disp3, float disp4) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *matrixP = createMatrix(4, 4, error);
    matrixP->matrix[0] = disp1;
    matrixP->matrix[5] = disp2;
    matrixP->matrix[10] = disp3;
    matrixP->matrix[15] = disp4;
    return matrixP;
}

Matrix *makePrediction(Matrix *matrixF, Matrix *matrixX, Matrix *matrixB, float controlValue) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *predictionXMatrix;
    predictionXMatrix = sumMatrix(mulMatrix(matrixF, matrixX, error), mulMatrixAndValue(matrixB, controlValue, error),
                                  error);
    return predictionXMatrix;
}

Matrix *makeProjectErrorCovariance(Matrix *matrixF, Matrix *matrixP, Matrix *matrixQ) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *projectErrorCovariance;
    projectErrorCovariance = sumMatrix(
            mulMatrix(mulMatrix(matrixF, matrixP, error), transposeMatrix(matrixF, error), error), matrixQ, error);
    return projectErrorCovariance;
}

Matrix *calculateKalmanCoefficient(Matrix *projectErrorCovarianceMatrixP, Matrix *matrixH, Matrix *matrixR) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *kalmanCoefficient;
    Matrix *s;
    Matrix *si;
    s = sumMatrix(
            mulMatrix(mulMatrix(matrixH, projectErrorCovarianceMatrixP, error), transposeMatrix(matrixH, error), error),
            matrixR, error);
    si = inverseMatrix(s, error);
    kalmanCoefficient = mulMatrix(projectErrorCovarianceMatrixP, mulMatrix(transposeMatrix(matrixH, error), si, error),
                                  error);
    freeMatrix(s);
    freeMatrix(si);
    return kalmanCoefficient;
}

Matrix *updatePrediction(Matrix *prediction, Matrix *kalmanCoefficient, Matrix *measurement, Matrix *matrixH) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *updatedPrediction;
    updatedPrediction = sumMatrix(prediction, mulMatrix(kalmanCoefficient, differenceMatrix(
            measurement, mulMatrix(matrixH, prediction, error), error), error), error);
    return updatedPrediction;
}

Matrix *
updateErrorCovarianceMatrixP(Matrix *kalmanCoefficient, Matrix *matrixH, Matrix *projectErrorCovarianceMatrixP) {
    int *error = (int *) malloc(sizeof(int));
    *error = 0;
    Matrix *errorCovarianceMatrixP = mulMatrix(differenceMatrix(makeTheIdentityMatrix(createMatrix(4, 4, error), error),
                                                                mulMatrix(kalmanCoefficient, matrixH, error), error),
                                               projectErrorCovarianceMatrixP, error);
    return errorCovarianceMatrixP;
}

Matrix *
oneStepKalmanFilter(Matrix *matrixF,
                    Matrix *matrixX,
                    Matrix *matrixB,
                    float controlValue,
                    Matrix *matrixP,
                    Matrix *matrixQ,
                    Matrix *matrixH,
                    Matrix *matrixR,
                    Matrix *measurement,
                    Matrix *returnXAndPMatrix) {
    Matrix *predictionX = makePrediction(matrixF, matrixX, matrixB, controlValue);
    Matrix *projectErrorCovarianceMatrixP = makeProjectErrorCovariance(matrixF, matrixP, matrixQ);
    Matrix *kalmanGain = calculateKalmanCoefficient(projectErrorCovarianceMatrixP, matrixH, matrixR);
    Matrix *updatedPrediction = updatePrediction(predictionX, kalmanGain, measurement, matrixH);
    Matrix *updatedMatrixP = updateErrorCovarianceMatrixP(kalmanGain, matrixH, projectErrorCovarianceMatrixP);
    returnXAndPMatrix[0] = *updatedPrediction;
    returnXAndPMatrix[1] = *updatedMatrixP;
    return returnXAndPMatrix;
}

Matrix *kalmanFilter(Matrix *matrixF,
                     Matrix *matrixX,
                     Matrix *matrixB,
                     float *controlValues,
                     int amountOfControlValues,
                     Matrix *matrixP,
                     Matrix *matrixQ,
                     Matrix *matrixH,
                     Matrix *matrixR,
                     Matrix *measurements,
                     Matrix *filteredValues) {
    Matrix *currentStepXAndP = malloc(sizeof(Matrix) * 2);
    Matrix *filteredX;
    Matrix *updatedP;
    currentStepXAndP = oneStepKalmanFilter(matrixF, matrixX, matrixB, controlValues[0], matrixP, matrixQ, matrixH,
                                           matrixR, &measurements[0], currentStepXAndP);
    filteredX = &currentStepXAndP[0];
    updatedP = &currentStepXAndP[1];
    filteredValues[0] = *filteredX;
    for (int i = 1; i < amountOfControlValues; i++) {
        currentStepXAndP = oneStepKalmanFilter(matrixF, filteredX, matrixB, controlValues[i], updatedP, matrixQ,
                                               matrixH, matrixR, &measurements[i], currentStepXAndP);
        filteredX = &currentStepXAndP[0];
        updatedP = &currentStepXAndP[1];
        filteredValues[i] = *filteredX;
    }
    return filteredValues;
}