#ifndef MATRIXCLION_RANDOM_H
#define MATRIXCLION_RANDOM_H

#include <math.h>
#include <stdlib.h>

float *normalRandom(float m, float sigma, float *randomNumber, int i);

#endif //MATRIXCLION_RANDOM_H
