#ifndef MATRIXCLION_UTILS_H
#define MATRIXCLION_UTILS_H

#include <stdio.h>
#include "Matrix.h"

void printMatrixInFile(FILE *fout, Matrix *matrix);

float myPow(float basis, int exponent);

#endif //MATRIXCLION_UTILS_H
