#ifndef MATRIXCLION_KALMANFILTER_H
#define MATRIXCLION_KALMANFILTER_H

#include <stdlib.h>
#include "Matrix.h"
#include "utils.h"

Matrix *createMatrixF(float dt);

Matrix *createMatrixH();

Matrix *createMatrixB(float dt);

Matrix *createDiscreteWhiteNoiseQMatrix(float dt);

Matrix *createMatrixR(float disp1, float disp2);

Matrix *createMatrixP(float disp1, float disp2, float disp3, float disp4);

Matrix *makePrediction(Matrix *matrixF, Matrix *matrixX, Matrix *matrixB, float controlValue);

Matrix *makeProjectErrorCovariance(Matrix *matrixF, Matrix *matrixP, Matrix *matrixQ);

Matrix *calculateKalmanCoefficient(Matrix *projectErrorCovarianceMatrixP, Matrix *matrixH, Matrix *matrixR);

Matrix *updatePrediction(Matrix *prediction, Matrix *kalmanCoefficient, Matrix *measurement, Matrix *matrixH);

Matrix *updateErrorCovarianceMatrixP(Matrix *kalmanCoefficient, Matrix *matrixH, Matrix *projectErrorCovarianceMatrixP);

Matrix *
oneStepKalmanFilter(Matrix *matrixF,
                    Matrix *matrixX,
                    Matrix *matrixB,
                    float controlValue,
                    Matrix *matrixP,
                    Matrix *matrixQ,
                    Matrix *matrixH,
                    Matrix *matrixR,
                    Matrix *measurement,
                    Matrix *returnXAndPMatrix
);

Matrix *kalmanFilter(Matrix *matrixF,
                     Matrix *matrixX,
                     Matrix *matrixB,
                     float *controlValues,
                     int amountOfControlValues,
                     Matrix *matrixP,
                     Matrix *matrixQ,
                     Matrix *matrixH,
                     Matrix *matrixR,
                     Matrix *measurements,
                     Matrix *filteredValues
);

#endif //MATRIXCLION_KALMANFILTER_H
