#ifndef MATRIXCLION_MATRIX_H
#define MATRIXCLION_MATRIX_H

/* Errors code */
// 1 - incorrect matrix size
// 2 - pointer to matrix equal NULL
// 3 - matrix is not square
// 4 - determinant equal zero

typedef struct Matrix {
    unsigned short int amountOfRows;
    unsigned short int amountOfColumns;
    float *matrix;
} Matrix;

Matrix *createMatrix(unsigned short int amountOfRows, unsigned short int amountOfColumns, int *error);

Matrix *createAndFillMatrixWithUserParametersAndUserValues(int *error);

Matrix *createMatrixWithUserParameters(int *error);

Matrix *makeTheIdentityMatrix(Matrix *matrixForTransformation, int *error);

Matrix *makeACopyOfMatrix(Matrix *originalMatrix, int *error);

Matrix *sumMatrix(Matrix *firstMatrix, Matrix *secondMatrix, int *error);

Matrix *differenceMatrix(Matrix *firstMatrix, Matrix *secondMatrix, int *error);

Matrix *transposeMatrix(Matrix *matrix, int *error);

Matrix *mulMatrix(Matrix *firstMatrix, Matrix *secondMatrix, int *error);

Matrix *mulMatrixAndValue(Matrix *matrix, float value, int *error);

float matrixDeterminant(Matrix *matrix, int *error); //Переделать на диагональный способ!

Matrix *inverseMatrix(Matrix *originalMatrix, int *error);

void printMatrix(Matrix *matrixForPrinting);

void freeMatrix(Matrix *matrix);

#endif //MATRIXCLION_MATRIX_H
